<div class="form-group mt-5">
    <label for="tipo">Tipo</label>
    <select id="tipo" class="form-control" name="tipo">
        <option>Calzado</option>
        <option>Informatica</option>
        <option>Celulares</option>
    </select>
</div>
<div class="form-group">
    <label for="modelo">Modelo</label>
    <input type="text" class="form-control" id="modelo" name="modelo" value="{{ isset($producto->modelo) ? $producto->modelo : '' }}">
</div>
<div class="form-group">
    <label for="marca">Marca</label>
    <input type="text" class="form-control" id="marca" name="marca" value="{{ isset($producto->marca) ? $producto->marca : '' }}">
</div>
<div class="form-group">
    <label for="precio">Precio</label>
    <input type="number" class="form-control" id="precio" name="precio" value="{{ isset($producto->precio) ? $producto->precio : '' }}">
</div>
<div class="form-group">
    <label for="img">Imagen</label>
    @if(isset($producto->img))
        <img src="{{ asset('storage'.'/'.$producto->img) }}" alt="" width="200">
    @endif
    <input type="file" class="form-control-file" id="img" name="img">
</div>
<a href="{{ url('/productos') }}" class="btn btn-danger">Cancelar</a>
<input type="submit" class="btn btn-primary" value="{{ $Modo == 'crear' ? 'Agregar' : 'Actualizar' }}">