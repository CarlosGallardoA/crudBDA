@extends('layouts.layout')
@section('title','ShopApp')
@section('css')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootswatch@4.5.2/dist/pulse/bootstrap.min.css" integrity="undefined" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.24/css/dataTables.bootstrap4.min.css">
@endsection
@section('content')
<br>
<table class="table table-striped table-bordered shadow-lg" id="products">
  <thead class="table table-dark">
    <tr>
      <th scope="col">#</th>
      <th scope="col">Imagen</th>
      <th scope="col">Tipo</th>
      <th scope="col">Modelo</th>
      <th scope="col">Marca</th>
      <th scope="col">Precio</th>
      <th scope="col">Acciones</th>
    </tr>
  </thead>
  <tbody>
      @foreach ($productos as $producto)
        <tr>
            <th scope="row">{{ $loop->iteration }}</th>
            <td><img src="{{ asset('storage').'/'.$producto->img }}" alt="" width="70" height="80" class="img-thumbnail img-fluid"></td>
            <td>{{ $producto->tipo }}</td>
            <td>{{ $producto->modelo }}</td>
            <td>{{ $producto->marca }}</td>
            <td>$.{{ $producto->precio }}</td>
            <td>
                <a href="{{ url('/productos/'.$producto->id.'/edit') }}" class="btn btn-warning">Editar</a>
                <form action="{{ url('/productos/'.$producto->id) }}" method="POST" class="d-inline">
                    @csrf
                    @method('DELETE')
                    <button type="submit" onclick="return confirm('Eliminar ?')" class="btn btn-danger">Eliminar</button>
                </form>
            </td>
        </tr>
      @endforeach
  </tbody>
</table>
@section('js')
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/dataTables.bootstrap4.min.js"></script>
<script>
    $(document).ready(function() {
    $('#products').DataTable();
} );
</script>
@endsection
@endsection